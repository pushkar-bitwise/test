﻿using System;
using System.Collections.Generic;

namespace Sample
{
    public struct Optional<T>
    {
        public bool HasValue { get; private set; }
        private T value;
        public T Value
        {
            get
            {
                if (HasValue)
                    return value;
                else
                    throw new InvalidOperationException();
            }
        }

        public Optional(T value)
        {
            this.value = value;
            HasValue = true;
        }

        public static explicit operator T(Optional<T> optional)
        {
            return optional.Value;
        }
        public static implicit operator Optional<T>(T value)
        {
            return new Optional<T>(value);
        }

        public override bool Equals(object obj)
        {
            if (obj is Optional<T>)
                return this.Equals((Optional<T>)obj);
            else
                return false;
        }
        public bool Equals(Optional<T> other)
        {
            if (HasValue && other.HasValue)
                return object.Equals(value, other.value);
            else
                return HasValue == other.HasValue;
        }
    }
    public abstract class TransactionMatcher
    {
        public abstract Transaction FindUniqueTransaction(CaseMessage input);

        protected Optional<List<Transaction>> ExecuteQuery(List<KeyValuePair<String, Object>> param)
        {
            return new List<Transaction>();
        }

        public abstract void setNextCondition(TransactionMatcher nextCondition);
    }

    public class TransactionMatcherWithCardAndARN : TransactionMatcher
    {
        private TransactionMatcher _nextCondition;
        public TransactionMatcherWithCardAndARN()
        {

        }
        public override Transaction FindUniqueTransaction(CaseMessage input)
        {
            var list = new List<KeyValuePair<string, object>>();
            list.Add(new KeyValuePair<string, object>("1", "1"));
            var result = ExecuteQuery(list);
            if (result.HasValue && result.Value.Count == 1)
            {
                return new Transaction();
            }


            return _nextCondition.FindUniqueTransaction(input);
        }

        public override void setNextCondition(TransactionMatcher nextCondition)
        {
            _nextCondition = nextCondition;
        }
    }

    public class TransactionMatcherWithTransactionDate : TransactionMatcher
    {
        private TransactionMatcher _nextCondition;
        public TransactionMatcherWithTransactionDate()
        {

        }
        public override Transaction FindUniqueTransaction(CaseMessage input)
        {
            throw new NotImplementedException();
        }


        public override void setNextCondition(TransactionMatcher nextCondition)
        {
            _nextCondition = nextCondition;
        }
    }


    public class TransactionMatcherWithAmount : TransactionMatcher
    {
        private TransactionMatcher _nextCondition;
        public TransactionMatcherWithAmount()
        {
        }
        public override Transaction FindUniqueTransaction(CaseMessage input)
        {
            throw new NotImplementedException();
        }


        public override void setNextCondition(TransactionMatcher nextCondition)
        {
            _nextCondition = nextCondition;
        }
    }


    public class TransactionMatcherWithTransactionCode : TransactionMatcher
    {
        private TransactionMatcher _nextCondition;
        public TransactionMatcherWithTransactionCode()
        {

        }
        public override Transaction FindUniqueTransaction(CaseMessage input)
        {
            throw new NotImplementedException();
        }


        public override void setNextCondition(TransactionMatcher nextCondition)
        {
            _nextCondition = nextCondition;
        }
    }

    public class TransactionMatcherWithAuthCode : TransactionMatcher
    {
        private TransactionMatcher _nextCondition;
        public TransactionMatcherWithAuthCode()
        {
        }
        public override Transaction FindUniqueTransaction(CaseMessage input)
        {
            throw new NotImplementedException();
        }


        public override void setNextCondition(TransactionMatcher nextCondition)
        {
            _nextCondition = nextCondition;
        }
    }

    public class Program
    {
        public bool Main()
        {
            var message = new CaseMessage();
            
            var condition1 = new TransactionMatcherWithCardAndARN();
            var condition2 = new TransactionMatcherWithTransactionDate();
            var condition3 = new TransactionMatcherWithAmount();
            var condition4 = new TransactionMatcherWithTransactionCode();
            var condition5 = new TransactionMatcherWithAuthCode();

            condition1.setNextCondition(condition2);
            condition2.setNextCondition(condition3);
            condition3.setNextCondition(condition4);
            condition4.setNextCondition(condition5);

            var uniqueTransaction = condition1.FindUniqueTransaction(message);

            return false;
        }
    }

}

