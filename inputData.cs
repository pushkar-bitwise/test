﻿namespace Sample
{
    public class CaseMessage
    {
        public string CardNumber { get; set; }
        public string ARN { get; set; }
        public string TransactionAmount { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionCode { get; set; }
        public string AuthCode { get; set; }
    }
}